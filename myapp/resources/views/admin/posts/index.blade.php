@extends('layouts.site.main')



@section('content')


    <hr>
    <h3>لیست مطالب منتشر شده</h3>

    @foreach($posts as $post)

        <div class="card border-primary mb-3" style="text-align: right">
            <div class="card-header">

                {{ $post->title }}

                <a href="{{ route('admin.post.edit',$post) }}"style="float: left">ویرایش</a>
            </div>
            <div class="card-body">
                <p class="card-text">{{ $post->content }}</p>
            </div>
        </div>

    @endforeach

@endsection