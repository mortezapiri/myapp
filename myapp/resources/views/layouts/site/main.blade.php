<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel</title>
    <link rel="stylesheet" href="/css/faltly/style.css">
</head>
<body>
<header>
    @include('layouts.admin.partials.header')
</header>
<main>
    <div class="container">
        @yield('content')
    </div>
</main>
</body>
</html>