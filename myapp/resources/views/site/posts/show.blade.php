@extends('layouts.site.main')

@section('content')

    <hr>

    <div class="jumbotron" style="text-align: right">


        {{--{{ Form::open(['method' => 'DELETE', 'route' => ['blog.delete', $post->id]]) }}--}}
        {{--{{ Form::hidden('id', $post->id) }}--}}
        {{--{{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}--}}
        {{--{{ Form::close() }}--}}


        {{ Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $post->id]]) }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger','style'=>'float : left']) }}
        {{ Form::close() }}

        <a href="{{ route('blog.like', ['id' => $post->id]) }}" name="like" methods="get">Like</a>

        <span>تعداد لایک ها{{ $post->likes_count }}</span>

        <h1 class="display-3">{{ $post->title }}</h1>

        <hr class="my-4">

        <p>{{ $post->content }}</p>

    </div>

    <hr>

    <h2>تعداد کامنت ها</h2>

    <h3>{{$total_post}}</h3>

    <h4>کامنت ها</h4>

    {!! form($form) !!}

    @foreach($comments as $comment)

        <div class="jumbotron" style="text-align: right">

            <p>{{ $comment->content }}</p>

        </div>

    @endforeach


@endsection