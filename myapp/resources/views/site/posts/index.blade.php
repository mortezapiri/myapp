@extends('layouts.site.main')

@section('content')
    {{--<h5 class="col-md-12" style="text-align: right">تعداد پست های منتشر شده{{ $posts->total() }}</h5>--}}
    <hr>
    @foreach($posts as $post)
        <div class="card border-primary mb-3" style="text-align: right">
            <div class="card-header">
                <span style="text-align: left; float: left"></span>

                <a href="{{ route('blog.show',$post) }}">{{ $post->title }}</a>

                <p>تعداد کامنت ها {{ $post->comments_count }}</p>





                {{--<a href="{{ route('blog.delete',$post->id) }}">حذف مقاله</a>--}}

                {{--<form method="delete" action="{{  route('posts.destroy',$post->id)  }}">--}}
                    {{--<div class="form-group">--}}
                        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

                        {{--<label for="dlt">Delete ID: </label>--}}
                        {{--<input name="id" class="form-control">--}}
                        {{--<div class="form-group">--}}
                            {{--<button type="submit" class="btn btn-primary">Delete</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</form>--}}


            </div>
            <div class="card-body">
                <p class="card-text">{{$post->content}}</p>
            </div>
        </div>
    @endforeach

    {!! $posts->render() !!}

@endsection