<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

use App\comment;

class CommentForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('content', 'textarea',[
                'label'  => 'توضیحات کامنت'
            ])
            ->add('submit', 'submit', [
                'label' => 'ارسال کامنت',
                'attr' => [
                    'class' => 'btn btn-success',
                ]
            ]);
    }
}
