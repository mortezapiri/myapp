<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Post;


class PostForm extends Form
{
    public function buildForm()
    {

        $this->add('title', 'text')
            ->add('content', 'textarea')
            ->add('saveAndReload ', 'submit', [
                'label' => 'ذخیره',
                'attr' => [
                    'class' => 'btn btn-success',
                    'default_value' => 'redirect',
                    'name' => 'action',
                    'value' => 'saveAndReload'
                ]
            ])
            ->add('saveAndClose', 'submit', [
                'label' => 'ذخیره و انتقال',
                'attr' => [
                    'class' => 'btn btn-success',
                    'default_value' => 'save',
                    'name' => 'action',
                    'value' => 'saveAndClose'
                ]
            ]);
    }

}
