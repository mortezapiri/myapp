<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Post;
use App\Comment;

class createPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create posts and Comments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0');
            Post::truncate();
            $this->info('Data Fetch Started');
            $posts = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/posts'));
            $this->info('Data Fetch Finished');

            $bar = $this->output->createProgressBar(count($posts));

            $this->info('Data Insert Started');
            foreach ($posts as $post) {
                \App\Post::create([
                    'title' => $post->title,
                    'content' => $post->body,
                ]);

                $bar->advance();
            }

            $bar->finish();
            $this->info('Data Insert Finished');


            Comment::truncate();
            $this->info('Data Fetch Started');
            $comments = json_decode(file_get_contents('https://jsonplaceholder.typicode.com/comments'));
            $this->info('Data Fetch Finished');

            $bar = $this->output->createProgressBar(count($comments));

            $this->info('Data Insert Started');
            foreach ($comments as $comment) {
                \App\Comment::create([
                    'post_id' => $comment->postId,
                    'content' => $comment->body,
                ]);
                $bar->advance();
            }

            $bar->finish();
            $this->info('Data Insert Finished');

            \DB::statement('SET FOREIGN_KEY_CHECKS=1');

        } catch (\Exception $e) {
            $this->error($e->getMessage() . ' - ' . $e->getFile() . '#' . $e->getLine());
        }


    }
}