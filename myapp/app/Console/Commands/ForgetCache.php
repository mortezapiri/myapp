<?php

namespace App\Console\Commands;

use App\Post;
use App\User;
use Illuminate\Console\Command;

class ForgetCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'forget:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete All Cache Post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {

            \Cache::flush();

            $this->info('all cache removed');

            $posts = Post::withCount('likes')->get();

            foreach ($posts as $post) {
                \Cache::forever('posts.' . $post->id, $post);
            }


        } catch (\Exception $e) {
            $this->error($e->getMessage() . ' - ' . $e->getFile() . '#' . $e->getLine());
        }


    }
}
