<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=['content','post_id'];

    protected $guarded=['name','email'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}