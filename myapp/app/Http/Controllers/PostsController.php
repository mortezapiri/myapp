<?php

namespace App\Http\Controllers\admin;

namespace App\Http\Controllers;

use App\Forms\PostForm;
use App\Post;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('site.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

            $form = \FormBuilder::create(PostForm::class, [
                'method' => 'POST',
                'url' => route('posts.store')
            ]);

            return view('admin.posts.form', compact('form'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Post::create($request->only('title', 'content'));
        return redirect()->route('posts.edit', ['id' => $post->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $post = Post::findOrFail($id);
            $form = \FormBuilder::create(PostForm::class, [
                'method' => 'PUT',
                'url' => route('posts.update', ['id' => $post->id]),
                'model' => $post,
            ]);


            return view('admin.posts.form', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->update($request->only('title', 'content'));

        \Cache::forget('posts.' . $post->id);

        $action = $request->input('action');

        switch ($action) {
            case 'saveAndReload':
                $return = redirect()->route('posts.edit', ['id' => $post->id]);
                break;
            case 'saveAndClose':
            default:
                $return = redirect()->route('blog.index');
        }
        return $return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

//        $post = Post::findOrFail($id);
//
//            $post->delete();
//
//            return Redirect::route('blog.index');

    }
}
