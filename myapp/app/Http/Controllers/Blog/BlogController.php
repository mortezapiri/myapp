<?php

namespace App\Http\Controllers\Blog;

use App\Comment;
use App\Forms\LikeForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Post;
use App\Forms\CommentForm;

use App\Like;

use Illuminate\Support\Facades\Cache;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::withCount('comments')->latest()->paginate(7);
        return view('site.posts.index', compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Cache::rememberForever('posts.' . $id, function () use ($id) {
            return Post::withCount('likes')->find($id);
        });
        if (!$post) {
            abort(404);
        }

        $form = \FormBuilder::create(CommentForm::class, [
            'method' => 'POST',
            'url' => route('blog.comment', ['id' => $post->id])
        ]);

        $comments = $post->comments;

        $total_post = $post->comments->count();

        return view('site.posts.show', compact('post', 'form', 'comments', 'total_post'));

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(request $request, $id)
    {

        $post = Post::findOrFail($id);

        $comment = $post->comments()->create($request->only('content'));

        return redirect()->route('blog.show', ['id' => $post->id]);

    }

    /**
     *
     */
    public function like($id)
    {
        Like::create([
            'post_id' => $id
        ]);

        Cache::forget('posts.' . $id);

        return redirect()->back();
    }


    public function destroy($id)
    {

        $post = Post::findOrFail($id);

        $post->delete();

        \Cache::forget('posts.' . $post->id);

        return redirect()->route('blog.index');

    }


}