<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Redis;


Route::get('/', function () {
    echo "Home";
});



Route::group(['prefix' => 'admin'], function () {
    Route::resource('posts', 'PostsController');
});

Route::group(['prefix' => 'blog', 'namespace' => 'Blog'], function () {
    Route::get('/posts', 'BlogController@index')->name('blog.index');
    Route::get('/posts/{id}', 'BlogController@show')->name('blog.show');
    Route::post('/posts/{id}/comment', 'BlogController@comment')->name('blog.comment');
    Route::get('/posts/{id}/like', 'BlogController@like')->name('blog.like');
    Route::delete('/posts/{id}', 'BlogController@destroy')->name('blog.destroy');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
